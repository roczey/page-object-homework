﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverHomework.Widgets
{
    class SimpleWidget : FactoryWidget<SimpleWidget>
    {
        public SimpleWidget(string cssSelector, IWebDriver driver) : base(cssSelector, driver)
        {
        }

        public SimpleWidget()
        {

        }
    }
}

﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverHomework.Widgets
{
    class ClickableWidget : FactoryWidget<ClickableWidget>
    {
        public ClickableWidget(string cssSelector, IWebDriver driver) : base(cssSelector, driver)
        {
        }

        public ClickableWidget()
        {
        }

        public void Click()
        {
            Element.Click();
        }
    }
}

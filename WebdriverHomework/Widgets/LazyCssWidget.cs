﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverHomework.Widgets
{
    class LazyCssWidget<T> : WidgetBase where T : LazyCssWidget<T>, new()
    {
        public string CssSelector { get; protected set; }

        private IWebElement element;

        public IWebElement Element
        {
            get
            {
                if (element == null)
                    element = WaitForDisplayedElement(CssSelector);

                return element;
            }
        }


        public LazyCssWidget(string cssSelector, IWebDriver driver) : base(driver)
        {
            CssSelector = cssSelector;
        }

        protected LazyCssWidget(IWebElement element, LazyCssWidget<T> parent) : this(parent.CssSelector, parent.Driver)
        {
            this.element = element;
        }

        protected LazyCssWidget()
        {
        }

        protected T Configure(IWebElement element, LazyCssWidget<T> parent)
        {
            this.element = element;
            this.CssSelector = parent.CssSelector;
            this.Driver = parent.Driver;

            return (T)this;
        }

        public bool FindSingleDisplayed()
        {
            try
            {
                return Element != null;
            }
            catch
            {
                return false;
            }
        }

        private IEnumerable<T> CreateChildren(IEnumerable<IWebElement> elements)
        {
            return elements.Select(element => new T().Configure(element, this));
        }

        public IEnumerable<T> AllInPage()
        {
            return CreateChildren(FindElements(CssSelector));
        }

        public IEnumerable<T> WaitForOneInPage()
        {
            return CreateChildren(WaitForOneOrMoreElements(CssSelector));
        }
    }
}

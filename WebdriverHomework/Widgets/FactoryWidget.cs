﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverHomework.Widgets
{
    class FactoryWidget<T> : LazyCssWidget<T> where T : FactoryWidget<T>, new()
    {
        public static T Construct(string cssSelector, IWebDriver driver)
        {
            return new T() { CssSelector = cssSelector, Driver = driver };
        }

        protected FactoryWidget(string cssSelector, IWebDriver driver) : base(cssSelector, driver)
        {
        }

        protected FactoryWidget()
        {
        }
    }
}

﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverHomework.Widgets
{
    abstract class WidgetBase
    {
        private IWebDriver driver;
        private WebDriverWait wait;

        protected IWebDriver Driver
        {
            get => driver;
            set {
                driver = value;
                wait = null;
            }
        }

        protected WebDriverWait Wait
        {
            get
            {
                if(wait == null)
                    wait = new WebDriverWait(driver, Config.WaitTimeout);

                return wait;
            }
        }



        public WidgetBase(IWebDriver driver)
        {
            this.driver = driver;
        }

        protected WidgetBase()
        {
        }

        protected IWebElement FindElement(string cssSelector)
        {
            return driver.FindElement(By.CssSelector(cssSelector));
        }
        protected ReadOnlyCollection<IWebElement> FindElements(string cssSelector)
        {
            return driver.FindElements(By.CssSelector(cssSelector));
        }

        protected IWebElement WaitForElement(string cssSelector)
        {
            return Wait.Until(driver => driver.FindElement(By.CssSelector(cssSelector)));
        }

        protected ReadOnlyCollection<IWebElement> WaitForOneOrMoreElements(string cssSelector)
        {
            return Wait.Until((driver) => {
                var elements = driver.FindElements(By.CssSelector(cssSelector));
                return elements.Any() ? elements : null;
            });
        }

        protected IWebElement WaitForDisplayedElement(string cssSelector)
        {
            return Wait.Until((driver) => {
                var elements = driver.FindElements(By.CssSelector(cssSelector));
                return elements.SingleOrDefault(x => x.Displayed);
            });
        }
    }
}

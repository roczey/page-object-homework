﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using WebdriverHomework.Pages;

namespace WebdriverHomework
{
    class BeadandoPageObjectTest : TestBase
    {
        [Test, TestCaseSource(nameof(NavigationTestDataLoader)), Order(1)]
        public void NavigationTest(NavigationTestData testData)
        {
            string testName = $"{nameof(NavigationTest)}_{testData.TestName}";

            ScreenshottableTest(testName, () =>
            {
                var page = RoyalRoadPage.Create(testData.Page, driver).Navigate();
                var clickableWidget = page.GetClickableWidget(testData.ClickTarget);

                clickableWidget.Click();

                var targetPage = RoyalRoadPage.Create(testData.ShouldNavigateTo, driver);
                Assert.That(targetPage.Verify(), Is.True);
            });
        }

        [Test, TestCaseSource(nameof(ClickTestDataLoader)), Order(2)]
        public void ClickTest(ClickTestData testData)
        {
            string testName = $"{nameof(ClickTest)}_{testData.TestName}";

            ScreenshottableTest(testName, () =>
            {
                var page = RoyalRoadPage.Create(testData.Page, driver).Navigate();
                var clickableWidget = page.GetClickableWidget(testData.ClickTarget);

                clickableWidget.Click();

                var revealedWidget = page.GetSimpleWidget(testData.ShouldReveal);
                Assert.That(revealedWidget.FindSingleDisplayed(), Is.True);
            });
        }

        static IEnumerable<T> LoadTestData<T>(string jsonFilePath)
        {
            string fileText = File.ReadAllText(jsonFilePath);
            return JsonSerializer.Deserialize<IEnumerable<T>>(fileText);
        }

        static IEnumerable<ClickTestData> ClickTestDataLoader()
        {
            return LoadTestData<ClickTestData>("clicktestdata.json");
        }
        static IEnumerable<NavigationTestData> NavigationTestDataLoader()
        {
            return LoadTestData<NavigationTestData>("navigationtestdata.json");
        }

        public class ClickTestData
        {
            public string TestName { get; set; }
            public string Page { get; set; }
            public string ClickTarget { get; set; }
            public string ShouldReveal { get; set; }
        }

        public class NavigationTestData
        {
            public string TestName { get; set; }
            public string Page { get; set; }
            public string ClickTarget { get; set; }
            public string ShouldNavigateTo { get; set; }
        }
    }
}

﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverHomework.Widgets;

namespace WebdriverHomework.Pages
{
    class WidgetFactoryPage : PageBase
    {
        public WidgetFactoryPage(IWebDriver driver) : base(driver)
        {
        }
        protected WidgetFactoryPage()
        {
        }

        public ClickableWidget GetClickableWidget(string cssSelector)
        {
            return new ClickableWidget(cssSelector, driver);
        }

        public SimpleWidget GetSimpleWidget(string cssSelector)
        {
            return new SimpleWidget(cssSelector, driver);
        }

        protected T LazyWidget<T>(string cssSelector, ref T widgetRef) where T : FactoryWidget<T>, new()
        {
            if(widgetRef == null)
                widgetRef = FactoryWidget<T>.Construct(cssSelector, driver);

            return widgetRef;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverHomework.Pages
{
    interface IVerifiablePage
    {
        public bool Verify();
    }
}

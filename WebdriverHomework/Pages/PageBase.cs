﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverHomework.Pages
{
    abstract class PageBase
    {
        protected IWebDriver driver;

        public PageBase(IWebDriver driver)
        {
            this.driver = driver;
        }

        protected PageBase()
        {
        }
    }
}

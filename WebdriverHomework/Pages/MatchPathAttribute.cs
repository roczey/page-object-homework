﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverHomework.Pages
{
    [AttributeUsage(AttributeTargets.Class)]
    class MatchPathAttribute : Attribute
    {
        private string path;

        public MatchPathAttribute(string path)
        {
            this.path = path;
        }

        public static Dictionary<string, Func<string, IWebDriver, RoyalRoadPage>> GetMapping()
        {
            return new Dictionary<string, Func<string, IWebDriver, RoyalRoadPage>>(
                Assembly.GetExecutingAssembly().GetTypes()
                .Select((type) =>
                {
                    var attribute = type.GetCustomAttribute<MatchPathAttribute>();
                    if (attribute == null) return null;

                    var constructFuncField = type.GetField("Construct", BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);
                    var constructFunc = (Func<string, IWebDriver, RoyalRoadPage>)constructFuncField.GetValue(null);

                    return new { Path = attribute.path, ConstructFunc = constructFunc };
                })
                .Where(x => x != null)
                .Select(x => new KeyValuePair<string, Func<string, IWebDriver, RoyalRoadPage>>(x.Path, x.ConstructFunc))
            );

        }
    }
}

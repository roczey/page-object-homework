﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverHomework.Widgets;

namespace WebdriverHomework.Pages
{
    [MatchPath("/account/login")]
    class LoginPage : FactoryPage<LoginPage>
    {
        private SimpleWidget emailInput;
        private SimpleWidget passwordInput;
        private ClickableWidget rememberInputLabel;
        private ClickableWidget signInButton;
        public SimpleWidget EmailInput { get => LazyWidget("input#email", ref emailInput); }
        public SimpleWidget PasswordInput { get => LazyWidget("input#password", ref passwordInput); }
        public ClickableWidget RememberInputLabel { get => LazyWidget("input#remember + label", ref rememberInputLabel); }
        public ClickableWidget SignInButton { get => LazyWidget(".login-divider ~ button.btn-primary", ref signInButton); }

        public override bool Verify()
        {
            return
                EmailInput.FindSingleDisplayed() &&
                PasswordInput.FindSingleDisplayed() &&
                RememberInputLabel.FindSingleDisplayed() &&
                SignInButton.FindSingleDisplayed() &&
                base.Verify();
        }
    }
}

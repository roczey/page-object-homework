﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverHomework.Pages
{
    class RoyalRoadPage : WidgetFactoryPage, IVerifiablePage
    {
        /*public static readonly Dictionary<string, Func<string, IWebDriver, RoyalRoadPage>> CustomPages = new()
        {
            ["/user/memberlist"] = MemberListPage.Construct
        };*/
        public static readonly Dictionary<string, Func<string, IWebDriver, RoyalRoadPage>> CustomPages =
            MatchPathAttribute.GetMapping();

        public const string BaseUrl = "https://www.royalroad.com";
        public string Path { get; protected set; }

        protected RoyalRoadPage(string path, IWebDriver driver) : base(driver)
        {
            Path = path;
        }

        protected RoyalRoadPage()
        {
        }

        public static RoyalRoadPage Create(string path, IWebDriver driver)
        {
            var baseUri = new Uri(BaseUrl);
            var uri = new Uri(baseUri, path);
            string pathPart = uri.AbsolutePath;

            CustomPages.TryGetValue(pathPart, out var construct);

            if(construct != null)
                return construct(path, driver);

            return new RoyalRoadPage(path, driver);
        }

        public RoyalRoadPage Navigate()
        {
            var baseUri = new Uri(BaseUrl);
            var uri = new Uri(baseUri, Path);

            driver.Navigate().GoToUrl(uri);

            return this;
        }

        public virtual bool Verify()
        {
            var baseUri = new Uri(BaseUrl);
            var targetUri = new Uri(baseUri, Path);

            var uri = new Uri(driver.Url);

            return uri.Host == targetUri.Host && uri.AbsolutePath == targetUri.AbsolutePath;
        }
    }
}

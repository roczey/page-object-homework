﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverHomework.Pages
{
    class FactoryPage<T> : RoyalRoadPage where T : FactoryPage<T>, new()
    {
        public static Func<string, IWebDriver, T> Construct =
            (string path, IWebDriver driver) => new T() { Path = path, driver = driver };

        protected FactoryPage(string path, IWebDriver driver) : base(path, driver)
        {
        }

        protected FactoryPage()
        {
        }
    }
}

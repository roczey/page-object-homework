﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverHomework.Widgets;

namespace WebdriverHomework.Pages
{
    [MatchPath("/fictions/search")]
    class SearchPage : FactoryPage<SearchPage>
    {
        private SimpleWidget searchInput;
        private ClickableWidget searchButton;
        public SimpleWidget SearchInput { get => LazyWidget(".search-page .search-bar input[name=title]", ref searchInput); }
        public ClickableWidget SearchButton { get => LazyWidget(".search-page .search-bar input[name=title] + span > button", ref searchButton); }

        public override bool Verify()
        {
            return
                SearchInput.FindSingleDisplayed() &&
                SearchButton.FindSingleDisplayed() &&
                base.Verify();
        }
    }
}

﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverHomework.Widgets;

namespace WebdriverHomework.Pages
{
    [MatchPath("/user/memberlist")]
    class MemberListPage : FactoryPage<MemberListPage>
    {
        private SimpleWidget usernameQueryInput;
        public SimpleWidget UsernameQueryInput { get => LazyWidget("input#q", ref usernameQueryInput); }

        public override bool Verify()
        {
            return
                UsernameQueryInput.FindSingleDisplayed() &&
                base.Verify();
        }
    }
}

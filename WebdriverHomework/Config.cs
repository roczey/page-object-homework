﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverHomework
{
    static class Config
    {
        public static readonly string[] ChromeArguments = { "--lang=hu", "--window-size=1920,1080" };
        public static readonly TimeSpan WaitTimeout = TimeSpan.FromSeconds(10);
    }
}

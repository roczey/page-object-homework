﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.IO;

namespace WebdriverHomework
{
    [TestFixture]
    abstract class TestBase
    {
        protected WebDriver driver;

        public WebDriver Driver
        {
            get => driver;
            set
            {
                driver?.Quit();
                driver = value;
            }
        }

        [SetUp]
        protected void Setup()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArguments(Config.ChromeArguments);

            Driver = new ChromeDriver(options);
        }

        [TearDown]
        protected void Teardown()
        {
            driver.Quit();
        }

        protected void ScreenshottableTest(string testName, Action test)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                test();
            }
            else
            {
                try
                {
                    test();
                }
                catch (Exception e)
                {
                    var screenshot = driver.GetScreenshot();
                    string screenshotPath = Path.GetFullPath($"FAIL_{DateTime.Now:yyyyMMdd_HHmm}_{testName}.jpg");
                    screenshot.SaveAsFile(screenshotPath, ScreenshotImageFormat.Jpeg);
                    throw new AggregateException($"{testName} failed, screenshot saved to\n{screenshotPath}\n", e);
                }
            }
        }
    }
}
